# createDevices.py creates a json file with devices for use in AT&T M2X system

import json

# Create a dictionary for each device, and add it to the list below. Include
# the phone number to text when an alarm is received, if enabled.
# The parser name gets looked up in rest2m2x.py. If it is new, you need to
# add it in rest2m2x.py as well.
# NOTE: to send a text to a cellphone, use this as the email address:
#    Ten_digit_number@X,
# X = vtext.com (verizon), text.att.net (att), tmomail.net (tmobile), messaging.sprintpcs.com (sprint)
# ex. ['8585551212@text.att.net']

devices = [
{'desc':'dragonDev1',
'nodeId':'0x72cfc',
'parser':'serial_1',
'm2x_device_id': '2cbb8822b9d5741dec283ce0e25dfa01',
'm2x_primary_key':'0747a49d34f020cab58f57b4169234da',
'alarm_email_enabled':0,
'alarm_email_list':[]},
           
{'desc':'Intrusion (GPIO Example)',
'nodeId':'0x56a38',
'parser':'intrusion_detector_1',
'm2x_device_id': '',
'm2x_primary_key':'', 
'alarm_email_enabled':0,
'alarm_email_list':[]},

{'desc':'Serial (UART Example)',
'nodeId':'0x56aa5',
'parser':'serial_1',
'm2x_device_id': '',
'm2x_primary_key':'', 
'alarm_email_enabled':0,
'alarm_email_list':[]},

{'desc':'Pulse Counter',
'nodeId':'0x69fe7',
'parser':'pulse_1',
'm2x_device_id': '',
'm2x_primary_key':'', 
'alarm_email_enabled':0,
'alarm_email_list':[]},

{'desc':'Temp/Humidty (4-20 mA A/D Example)',
'nodeId':'0x56a37',
'parser':'temperature_humidity_1',
'm2x_device_id': '',
'm2x_primary_key':'', 
'alarm_email_enabled':0,
'alarm_email_list':[]},

{'desc':'DTRACK (GPS Example)',
'nodeId':'0x24b4',
'parser':'gps_2',
'm2x_device_id': '',
'm2x_primary_key':'', 
'alarm_email_enabled':0,
'alarm_email_list':[]},
]

with open('Devices.json', 'w') as outfile:
   json.dump(devices, outfile)

print "create_devices: Done"
