#!/bin/env python
# getDeviceInfo.py look up information

# Returns a dictionary, with parser info and m2x keys for nodeId
def getDeviceInfo(node_id_hex, devices):
    #match_found = 0 # no match found
    match_found = []
    ret_dict = [] 
    for ii in range(len(node_id_hex)):

        # Assume nothing found initially
        ret_dict.append({})
        match_found.append(0)

        for device in devices:
            if int(device['nodeId'], 16) == int(node_id_hex[ii], 16):
                # Device found
                ret_dict[-1] = device
                match_found[-1] = 1

    return (match_found, ret_dict)

