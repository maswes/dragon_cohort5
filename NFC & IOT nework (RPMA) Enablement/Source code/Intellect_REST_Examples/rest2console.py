# rest2console.py Pulls UL SDUs from ORW REST interface and prints to screen. 
# Pull an SDU, Look up what parser to use, Parse the SDU, For each measurement in the SDU print to screen. 

import sys
import time
import datetime
import json
from sendStream2Console import * 
from getNextUlSdu import getNextUlSdu
from getNextUlSdu import incrementNextUlSduPointer
from getDeviceInfo import getDeviceInfo
import parsers
from optparse import OptionParser
from restUtils import *
import pdb
from m2x.client import M2XClient #Priya


# Configure command line parser.  Make global for simplicity of access
parser = OptionParser(description = 'Example REST parser for command line.')
parser.add_option('-n','--nodeid',dest='nodeIdToDisplay', 
  help='Print only this node specified by the hexadecimal node ID', default='0x0')
parser.add_option('-s','--startTime',dest='startTime', 
  help='Pull REST data starting from the time specified in this format HH:MM:SS. If left unspecified, the starting time is approximately two hours before now.')
parser.add_option('-S','--startDate',dest='startDate', 
  help='Pull REST data starting from date specified in this format YYYY-MM-DD. If left unspecified, the starting day is today.')
parser.add_option('-D','--dstAdj',dest='dstAdj', action="store_true", 
  help='Adjust internal time base for daylight savings time.  If left unspecified, daylight standard time is assumed.')
parser.add_option('-v','--verbosity',dest='logLevel', type=int, 
  help='0 - Level 0 (Error) Default, 1 - Level 1 (Error & Info)', default=0)

(globOpts, args) = parser.parse_args() 

# Set default start time if not specified 
tzOffset = datetime.timedelta(hours = time.timezone/3600.0)  # Offset in Standard Time 
if (globOpts.dstAdj):
    tzOffset = tzOffset - datetime.timedelta(hours = 1)
    dstString = "Local Daylight Savings"
else:
    dstString = "Local Daylight Standard"

# If they don't specify a start time, automatically rewind the start time
# from now.  This is to guarantee the main loop doesn't get stuck if the DST 
# flag isn't set properly.
timeRewind = datetime.timedelta(hours = 2)
startTimeDateDefault = datetime.datetime.today() - timeRewind


if (globOpts.startDate == None):
    year = startTimeDateDefault.year
    month = startTimeDateDefault.month
    day = startTimeDateDefault.day
else:
    dateTokens = globOpts.startDate.split('-')
    year = int(dateTokens[0])
    month = int(dateTokens[1])
    day = int(dateTokens[2]) 

if (globOpts.startTime == None):
    hh = startTimeDateDefault.hour 
    mm = startTimeDateDefault.minute 
    ss = startTimeDateDefault.second 
else:
    timeTokens = globOpts.startTime.split(':')
    hh = int(timeTokens[0])
    mm = int(timeTokens[1])
    ss = int(timeTokens[2])
  
    
startDateTime = datetime.datetime(year, month, day, hh, mm, ss)
tmpStr = "Collecting REST data starting: %s (%s)" % (startDateTime, dstString) 
startDateTime_UTC = startDateTime + tzOffset  # From this point forward, manage as UTC
consolePrint(tmpStr, CPM_REQ) 


def parsingLogic(dev_info, sdu_id, node_id_hex, rx_timestamp, payload):
    # Call a parser with the payload
    print '------------------------------------------------------------------------------\n'
    print 'Begin parsing ULSDU at %s\nparser: %s\nsdu_id: %s\nnodeId: %s\nrx_timestamp: %s\npayload: %s'\
      % (datetime.datetime.now(), dev_info['parser'], sdu_id, hex(int(node_id_hex, 16)), rx_timestamp, payload)

    # ---------------------------------------------------------------------------------------------------
    # Serial 
    # ---------------------------------------------------------------------------------------------------
    if dev_info['parser'] == 'serial_1':
        (msgType, data) = parsers.parser_serial_1(payload)
        
        # Note we do not send the test button alarm to Console, so we only accept one message type
        if msgType == 'Serial':
            allMeasAccepted = 1 # default means Console accepted all measurements
            for i in range(len(data)):
                timestamp = convertToDateTime(rx_timestamp).strftime("%Y-%m-%dT%H:%M:%SZ") # format Console expects
                text = hex2text(data[i]['data']) # convert bytes to human readable text
                res = sendSerial2Console(dev_info['m2x_primary_key'], dev_info['m2x_device_id'],\
                                         data[i]['sensorName'], timestamp, text)
        
            # Priya Decoder start
            # Instantiate a client
            client = M2XClient(dev_info['m2x_primary_key'])
            device = client.device(dev_info['m2x_device_id'])
            
            text_spl = text.split('>')
            #print text_spl

            try:
                temp_stream = device.stream(text_spl[0])
                temp_stream.add_value(text_spl[1], timestamp=rx_timestamp)
            except:
                pass
            #end
            
            # We only increment next SDU if the current SDU made it to Console completely
            # or in the case of certain errors
            if allMeasAccepted:
                incrementNextUlSduPointer(nextSduFile, last_sdu_id)
        elif msgType == 'Alarm' and data['alarmType'] == 'TestButton':
            validAlarmType = 0
            print '*******************************************************'
            print 'Test Button Alarm Detected at %s' % data['timeStamp']
            print '*******************************************************'
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console
        else:
            print 'INFO: msgType will not be sent to Console: ', msgType
            print 'Data: ', data
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we discard  and don't send to Console

    # ---------------------------------------------------------------------------------------------------
    # Temperature Humidity 4-20mA Omega
    # Note Over the air alarms are not used in this example code, therefore if we receive any
    # alarm messages, they are ignored. Alarms (triggers) may be implemented in Console
    # ---------------------------------------------------------------------------------------------------
    elif dev_info['parser'] == 'temperature_humidity_1':
        (msgType, data) = parsers.parser_temperature_humidity_1(payload)
        if msgType == 'SensorData':
            allMeasAccepted = 1 # default means Console accepted all measurements
            for i in range(len(data)):
                timestamp = data[i]['timeStamp'].strftime("%Y-%m-%dT%H:%M:%SZ") # format Console expects
                res = sendStream2Console(dev_info['m2x_primary_key'], dev_info['m2x_device_id'],\
                                         data[i]['sensorName'], timestamp, data[i]['data'])
   
            # We only increment next SDU if the current SDU made it to Console completely
            if allMeasAccepted:
                incrementNextUlSduPointer(nextSduFile, last_sdu_id)
        elif msgType == 'Alarm' and data['alarmType'] == 'TestButton':
            validAlarmType = 0
            print '*******************************************************'
            print 'Test Button Alarm Detected at %s' % data['timeStamp']
            print '*******************************************************'
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console
        else:
            print 'INFO: msgType will not be send to Console: ', msgType
            print 'INFO: Data= ',  data
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console

    # ---------------------------------------------------------------------------------------------------
    # INTRUSION DETECTOR1 (Normally Closed Switch) & PROCESSOR TEMPERATURE
    # Note we re-map over the air alarm messages to sensor data, to allow triggers in Console
    # to do the alarming
    # ---------------------------------------------------------------------------------------------------
    elif dev_info['parser'] == 'intrusion_detector_1':
        (msgType, data) = parsers.parser_intrusion_detector_1(payload)
   
        # For Console testing/demo only, we convert alarm into data stream time series value. We
        # do this so Console will trigger the alarm.
        # Ignore the "interrupt" pushbutton, by not parsing those alarms
        validAlarmType = 0
        if msgType == 'Alarm' and data['alarmCnt'] != '01':
            print 'ERROR: Greater than 1 alarm per message not supported. Not sent to Console!!! Data: ',  data
            print 'Try increasing hysteresis so only 1 alarm is sent per SDU.'
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console
        elif msgType == 'Alarm' and data['alarmType'] == 'TestButton':
            validAlarmType = 0
            print '*******************************************************'
            print 'Test Button Alarm Detected at %s' % data['timeStamp']
            print '*******************************************************'
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console
        elif msgType == 'Alarm' and data['alarmType'] == 'AppIntf1' and data['digAlarmThresh']=='Active_High':
            validAlarmType = 1
            if data['alarmState']=='Set':
                alarm_data = [{'sensorName':'intrusion', 'timeStamp':data['timeStamp'], 'data':1}]
            elif data['alarmState']=='Cleared':
                alarm_data = [{'sensorName':'intrusion', 'timeStamp':data['timeStamp'], 'data':0}]
            data = alarm_data # overwrite data with only the stuff we need to send to Console...
        elif msgType == 'SensorData':
            pass # SensorData is already in the correct format for sendStream2Console
        else:
            print 'INFO: msgType will not be sent to Console: ', msgType
            print 'INFO: Data= ',  data
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console

        if msgType == 'SensorData' or validAlarmType == 1:
            allMeasAccepted = 1 # default means Console accepted all measurements
            for i in range(len(data)):
                timestamp = data[i]['timeStamp'].strftime("%Y-%m-%dT%H:%M:%SZ") # format Console expects
                res = sendStream2Console(dev_info['m2x_primary_key'], dev_info['m2x_device_id'],\
                                         data[i]['sensorName'], timestamp, data[i]['data'])
   
            # We only increment next SDU if the current SDU made it to Console completely
            if allMeasAccepted:
                incrementNextUlSduPointer(nextSduFile, last_sdu_id)
   
    # ---------------------------------------------------------------------------------------------------
    # INTRUSION DETECTOR2 (Normally Open Switch) & PROCESSOR TEMPERATURE
    # Note we re-map over the air alarm messages to sensor data, to allow triggers in Console
    # to do the alarming
    # Note we invert intrusion data, too (flipBit)
    # ---------------------------------------------------------------------------------------------------
    elif dev_info['parser'] == 'intrusion_detector_2':
        (msgType, data) = parsers.parser_intrusion_detector_2(payload)
   
        # On the way into Console we convert sign of intrusion detector from active low to active high for
        # graphing (so 1=intrusion)
        if msgType == 'SensorData':
            for i in range(len(data)):
                if data[i]['sensorName']=='intrusion':
                    data[i]['data'] = flipBit(data[i]['data'])
   
        # For Console testing/demo only, we convert alarm into data stream time series value. We
        # do this so Console will trigger the alarm.
        elif msgType == 'Alarm' and data['alarmCnt'] != '01':
            print 'ERROR: Greater than 1 alarm per message not supported. Not sent to Console!!! Data: ',  data
            print 'Try increasing hysteresis so only 1 alarm is sent per SDU.'

        # Ignore the "interrupt" pushbutton, by not parsing exception alarms
        elif msgType == 'Alarm' and data['alarmType'] == 'TestButton':
            validAlarmType = 0
            print '*******************************************************'
            print 'Test Button Alarm Detected at %s' % data['timeStamp']
            print '*******************************************************'
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console
        elif msgType == 'Alarm' and data['alarmType'] == 'AppIntf2'\
            and data['digAlarmThresh']=='Active_Low':
            validAlarmType = 1
            # note inversion of data in two places below
            if data['alarmState']=='Set':
                alarm_data = [{'sensorName':'intrusion', 'timeStamp':data['timeStamp'], 'data':1}]
            elif data['alarmState']=='Cleared':
                alarm_data = [{'sensorName':'intrusion', 'timeStamp':data['timeStamp'], 'data':0}]
            data = alarm_data # overwrite data with only the stuff we need to send to Console. Regret: has caused me much confusion
   
        else:
            print 'INFO: msgType will not be sent to Console: ', msgType
            print 'INFO: Data= ',  data
            validAlarmType = 0
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console

        if msgType == 'SensorData' or validAlarmType == 1:
            allMeasAccepted = 1 # default means Console accepted all measurements
            for i in range(len(data)):
                timestamp = data[i]['timeStamp'].strftime("%Y-%m-%dT%H:%M:%SZ") # format Console expects
                res = sendStream2Console(dev_info['m2x_primary_key'], dev_info['m2x_device_id'],\
                                         data[i]['sensorName'], timestamp, data[i]['data'])
   
                # We only increment next SDU if the current SDU made it to Console completely
                if allMeasAccepted:
                    incrementNextUlSduPointer(nextSduFile, last_sdu_id)
   
    # ---------------------------------------------------------------------------------------------------
    # GPS LAT/LNG & DOWNLINK RSSI
    # Notes:
    # 1) This is not a stream in Console. We update the device location (waypoints)
    # 2) We use the 'elevation' attribute to report DL RSSI in dBm (-132dB is edge of cell)
    # 3) We implement decimation in what is reported to Console to avoid rate limiting, we measure every 4.5
    #    seconds, and report one location every minute.
    # 4) Discard incorrect GPS values (including (0,0) means no fix)
    # ---------------------------------------------------------------------------------------------------
    elif dev_info['parser'] == 'gps_2':
        (msgType, data) = parsers.parser_gps_2(payload)

        # Rare Case of deprecated RACM alarm message (opcode 0x01)
        if msgType == 'ERROR':
            print "ERROR: GPS dropped unexpected length. This could be a RACM alarm message ", sdu_id, rx_timestamp, payload
            incrementNextUlSduPointer(nextSduFile, last_sdu_id)
            allMeasAccepted = 0

        elif msgType == 'RfdtData':
            allMeasAccepted = 1 # default means Console accepted all measurements
            # Expect one location data but this is generalized to list of N
            for i in range(len(data)):
                gps_ok = isGpsValid(data[i]['lat'], data[i]['lng'])
                nodeIdHex = hex(int(node_id_hex, 16))
                timestamp = convertToDateTime(rx_timestamp)
                if gps_ok:
                    should_decimate = shouldDecimate(nodeIdHex, 
                      timestamp, gpsHistoryList)
                    # KS:  No real need to decimate in console version?
                    #if not should_decimate:
                    if (1):
                        timestamp = timestamp.strftime("%Y-%m-%dT%H:%M:%SZ") # format Console expects
                        sendLocRssi2Console(dev_info['m2x_primary_key'], dev_info['m2x_device_id'],\
                                              timestamp, data[i]['lat'], data[i]['lng'], data[i]['rssi'])
                    #else:
                    #   print "INFO: GPS dropped (decimated) to reduce rate", data[i]['sensorName'],datetime.datetime.now(),\
                    #      hex(int(node_id_hex, 16)), data[i]['lat'], data[i]['lng'], data[i]['rssi'], timestamp
                    #   incrementNextUlSduPointer(nextSduFile, last_sdu_id)
                    #   allMeasAccepted = 0
                else:
                    print "INFO: GPS dropped due to invalid location", data[i]['sensorName'],datetime.datetime.now(),\
                      hex(int(node_id_hex, 16)), data[i]['lat'], data[i]['lng'], data[i]['rssi'], timestamp
                    incrementNextUlSduPointer(nextSduFile, last_sdu_id)
                    allMeasAccepted = 0

            # We only increment next SDU if the current SDU made it to Console completely
            # or in the case of certain errors
            if allMeasAccepted:
                incrementNextUlSduPointer(nextSduFile, last_sdu_id)
        elif msgType == 'Alarm' and data['alarmType'] == 'TestButton':
            validAlarmType = 0
            print '*******************************************************'
            print 'Test Button Alarm Detected at %s' % data['timeStamp']
            print '*******************************************************'
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console
        else:
            print 'INFO: msgType will not be send to Console: ', msgType
            print 'INFO: Data= ',  data
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console

    # ---------------------------------------------------------------------------------------------------
    # Pulse Counter 
    # Note Over the air alarms are not used in this example code, therefore if we receive any
    # alarm messages, they are ignored. Alarms (triggers) may be implemented in Console
    # ---------------------------------------------------------------------------------------------------
    elif dev_info['parser'] == 'pulse_1':
        (msgType, data) = parsers.parser_pulse_1(payload)
        if msgType == 'SensorData':
            allMeasAccepted = 1 # default means Console accepted all measurements
            for i in range(len(data)):
                timestamp = data[i]['timeStamp'].strftime("%Y-%m-%dT%H:%M:%SZ") # format Console expects
                res = sendStream2Console(dev_info['m2x_primary_key'], dev_info['m2x_device_id'],\
                                         data[i]['sensorName'], timestamp, data[i]['data'])

            # We only increment next SDU if the current SDU made it to Console completely
            if allMeasAccepted:
                incrementNextUlSduPointer(nextSduFile, last_sdu_id)
        elif msgType == 'Alarm' and data['alarmType'] == 'TestButton':
            validAlarmType = 0
            print '*******************************************************'
            print 'Test Button Alarm Detected at %s' % data['timeStamp']
            print '*******************************************************'
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console
        else:
            print 'INFO: msgType will not be send to Console: ', msgType
            print 'INFO: Data= ',  data
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console

    # ---------------------------------------------------------------------------------------------------
    # KZCO demo parser 
    # ---------------------------------------------------------------------------------------------------
    elif dev_info['parser'] == 'KZCO_1':
        (msgType, data) = parsers.parser_KZCO_1(payload)
        if msgType == 'SensorData':
            for i in range(len(data)):
                timestamp = data[i]['timeStamp'].strftime("%Y-%m-%dT%H:%M:%SZ") # format Console expects
                res = sendStream2Console(dev_info['m2x_primary_key'], dev_info['m2x_device_id'],\
                                         data[i]['sensorName'], timestamp, data[i]['data'])

                incrementNextUlSduPointer(nextSduFile, last_sdu_id)

        elif msgType == 'Alarm':
            if data['alarmCnt'] != '01':
                print 'ERROR: Greater than 1 alarm per message not supported. Not sent to Console!!! Data: ',  data
                print 'Try increasing hysteresis so only 1 alarm is sent per SDU.'

                incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console

            elif data['alarmType'] == 'AppIntf6':
                if data['alarmState']=='Set':
                    alarmValue = 1
                elif data['alarmState']=='Cleared':
                    alarmValue = 0

                timestamp = data['timeStamp'].strftime("%Y-%m-%dT%H:%M:%SZ") # format Console expects
                res = sendStream2Console(dev_info['m2x_primary_key'], dev_info['m2x_device_id'],\
                                         'Alarm Event '+data['alarmType'], timestamp, alarmValue)

                incrementNextUlSduPointer(nextSduFile, last_sdu_id)

        else:
            print 'INFO: msgType will not be send to Console: ', msgType
            print 'INFO: Data= ',  data
            incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console

    # ---------------------------------------------------------------------------------------------------
    # Unknown Parser
    # ---------------------------------------------------------------------------------------------------
    else:
        print "ERROR: Can't find specified parser: %s for SDU_ID:%s nodeId=%s,rx_timestamp=%s,payload=%s"\
          % (dev_info['parser'], sdu_id, hex(int(node_id_hex, 16)), rx_timestamp, payload)
   

# -------------------------------------------------------------------------------------------
# MAIN
# -------------------------------------------------------------------------------------------
# This file contains the SDU ID of the next UL SDU. It provides non-volatile storage,
# so we only fetch the latest SDUs, if this script is stopped for any reason.


nextSduFile = 'lastFetchedMssgId.json'
gpsHistoryList = [] # list of dicts timestamp vs node ID for decimation

with open('Devices.json','r') as infile:
    devices = json.load(infile)
    infile.close()

while 1:
   try:
      # Polls REST interface for next 10 UL SDU's in Intellect since last poll.
      (status, last_sdu_id, size, sdu_list) = \
        getNextUlSdu(nextSduFile, startDateTime_UTC, 500, globOpts.logLevel)

      if status != 'OK':
          print 'sleep on error'
          time.sleep(10) # problem, wait and try again
          print 'done sleep on error'
      else:
          if size == 0 and last_sdu_id == '':
              time.sleep(1) # queue is empty, attempt to get latest SDUs every 1 second
              continue
          elif size == 0:
              print 'DownlinkDatagramResponse (or non-uplink mssg) detected.'
              incrementNextUlSduPointer(nextSduFile, last_sdu_id)
          else:
              (sdu_id, node_id_hex, rx_timestamp, payload) = getSduInfo(sdu_list)     
              (match_found, dev_info) = getDeviceInfo(node_id_hex, devices)

              for ii in range(len(match_found)):
                  if match_found[ii]:
                      if ((int(globOpts.nodeIdToDisplay, 16) == 0x0) or
                         (int(globOpts.nodeIdToDisplay, 16) == int(node_id_hex[ii], 16))):
                          parsingLogic(dev_info[ii], sdu_id[ii], 
                            node_id_hex[ii], rx_timestamp[ii], payload[ii])
                  else:
                      tmpStr = "No device found for SDU ID: %s, NID: %s, Rx Time: %s, Payload: 0x%s" % \
                        (sdu_id[ii], node_id_hex[ii], rx_timestamp[ii], payload[ii])
                      consolePrint(tmpStr, CPM_INFO, globOpts.logLevel) 
                      incrementNextUlSduPointer(nextSduFile, last_sdu_id) # increment if we don't send to Console

          # startDateTime_UTC is never used from this point forward, so invalidate
          startDateTime_UTC = None

   except KeyboardInterrupt:
      sys.exit()
